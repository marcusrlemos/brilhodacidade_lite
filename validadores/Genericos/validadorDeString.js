var validadorDeString = {
  ehVazioOuNulo: function(valor) {
    return !valor || !valor.trim();
  },
  ehMenorQueOTamanhoMinimo: function(valor,  tamanhoMinimo) {
    return valor.length < tamanhoMinimo;
  },
  ehMaiorQueOTamanhoMaximo: function(valor,  tamanhoMaximo) {
    return valor.length > tamanhoMaximo;
  }
};

module.exports = validadorDeString;
