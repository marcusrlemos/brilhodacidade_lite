var validadorDeString = require('./Genericos/validadorDeString');
var dadosInvalidosException = require('../exceptions/dadosInvalidosException');

function validadorDeEvento(evento) {
  var self = this;
  self.entidade = evento;

  self.validar = function() {
    verificacaoDeStringNulaOuVazia();
  };

  function verificacaoDeStringNulaOuVazia() {
    if (validadorDeString.ehVazioOuNulo(self.entidade.titulo))
      throw new dadosInvalidosException("O titulo é obrigatório");

    if (validadorDeString.ehMenorQueOTamanhoMinimo(self.entidade.titulo, 100))
      throw new dadosInvalidosException("O titulo deve possuir até 100 caracteres");

    if (self.entidade.descricao && validadorDeString.ehMenorQueOTamanhoMinimo(self.entidade.descricao, 1000))
      throw new dadosInvalidosException("A descrição do evento não pode possuir mais que 1000 caracteres");
  }
};

module.exports = validadorDeEvento;
