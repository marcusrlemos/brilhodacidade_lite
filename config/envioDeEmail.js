var nodemailer = require('nodemailer');
var fs = require('fs');
var path = require('path');
var _ = require('underscore');

//_.templateSettings.interpolate = /\{\{(.+?)\}\}/g;

function _obterTransporter() {
    return nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'marcusrlemos@gmail.com',
            pass: 'tixzcbzrdmursxme'
        }
    }, {
        // default values for sendMail method
        from: {
            name: 'Brilho da Cidade',
            address: 'marcusrlemos@gmail.com'
        },
        headers: { }
    });
}

function _obterTemplate(templateName) {
    templatePath = global.diretorio + "/emails/" + templateName + ".html";

    try {
        _.templateSettings = {
            interpolate: /\{\{(.+?)\}\}/g
        };

        templateContent = fs.readFileSync(templatePath, {encoding: "utf8"});
        return _.template(templateContent);
    } catch (e) {
        throw new Error("Erro :(");
    }
}

function _enviarEmail(enviarPara, assunto, template, dados) {
    var transporter = _obterTransporter();
    var templateHtml = _obterTemplate(template);

    try {
        transporter.sendMail({
            to: enviarPara,
            subject: assunto,
            html: templateHtml(dados)
        });
    } catch (e) {
        throw e;
    }
}

var envioDeEmail = {
    enviarEmail: _enviarEmail
};

module.exports = envioDeEmail;