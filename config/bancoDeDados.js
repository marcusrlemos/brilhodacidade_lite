var mysql = require("mysql");
var q = require('q');

function _obterConexao() {
    return mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "123456",
        database: 'brilhodacidade_lite'
    });
}

function _verificarConexao() {
    var con = _obterConexao();

    con.connect(function(err){
        if(err){
            console.log('Error connecting to Db');
            return;
        }
        console.log('Connection established');
    });

    con.end();
}

function _executar(sql) {
    var d = q.defer();
    var con = _obterConexao();

    con.query(sql, function(err, results) {
        if(err) throw err;
        d.resolve(results)
    });

    return d.promise;
}

function _obterLista(entidade, regras, propriedades) {
    if(!propriedades) propriedades = '*';
    var d = q.defer();
    var con = _obterConexao();

    var colunas = '';
    if(propriedades && typeof propriedades === 'array'){
        for(var i = 0; i < propriedades.length; i++){
            if(i != 0) colunas += ',';
            colunas += propriedades[i];
        }
    } else
        colunas = propriedades;

    var where = '';
    if(regras) {
        where  = ' where';

        for(var i = 0; i < regras.length; i++){
            where += ' ' + regras[i];
        }
    }

    var query = con.query('SELECT ' + colunas + ' FROM ' + entidade  + where, [propriedades, entidade], function(err, results) {
        if(err) throw err;
        d.resolve(results);
    });

    return d.promise;
}

function _obterPorPropriedade(entidade, propriedade, valor){
    var d = q.defer();
    var con = _obterConexao();

    var query = con.query(
        'SELECT * FROM ' + entidade + ' WHERE ' + propriedade + ' = ?', valor,
        function(err, results) {
            if(err) throw err;
            d.resolve(results.length > 0 ? results[0] : null);
        }
    );

    return d.promise;
}

function _existePorPropriedade(entidade, propriedade, valor){
    var d = q.defer();
    var con = _obterConexao();

    var query = con.query(
        'SELECT ?? FROM ?? WHERE ?? = ?', [propriedade, entidade, propriedade, valor],
        function(err, results) {
            if(err) throw err;
            d.resolve(results.length > 0);
        }
    );

    return d.promise;
}

var bancoDeDados = {
    verificarConexao: _verificarConexao,
    executar: _executar,
    obterLista: _obterLista,
    obterPorPropriedade: _obterPorPropriedade,
    existePorPropriedade: _existePorPropriedade
};

module.exports = bancoDeDados;