$(function(){

    $('#principal').parallax({
        imageSrc: '/images/background_pagina_interna.png'
    });

    $('.sobre-navegacao a').click(function() {
        var parte = $(this).attr('parte');
        var atualAtivo = $('.sobre-navegacao a[class=ativo]');

        atualAtivo.removeClass('ativo');
        $(this).addClass('ativo');

        $('.sobre-pagina[parte=' + atualAtivo.attr('parte') + ']').slideToggle('slow', function(){
            $('.sobre-pagina[parte=' + parte + ']').slideToggle('slow');
        });
    });

    //$('.texto-sobre').jScrollPane();

    $(window).scroll(function() {
        if ($(this).scrollTop() > 1) {
            $('header').addClass("sticky");
        }
        else {
            $('header').removeClass("sticky");
            setTimeout(function () {
                jQuery(window).trigger('resize').trigger('scroll');
            }, 200);
        }
    });

    $('.smoothScroll').click(function() {
        var element = '#' + $(this).attr('area');
        $('html, body').stop().animate({
            scrollTop:  $(element).offset().top - 135
        }, 1000);
    });

    function enviarEmail(_nome, _email, _mensagem) {
        return http()
            .post('/enviarEmail', { nome: _nome, email: _email, mensagem: _mensagem})
            .then(function (retorno) {
                return retorno;
            });
    }

    $('button.enviarMensagem').click(function() {
        var email = $('#contato_email').val();
        var nome = $('#contato_nome').val();
        var mensagem = $('#contato_mensagem').val();

        if (!nome || nome == '') {
            swal("Opsss...", "Como você gostaria de se chamar? É so informar pra gente seu nome :)", "error");
            return false;
        }

        if (!email || email == '') {
            swal("Opsss...", "Precisamos de um e-mail seu para te responder!", "error");
            return false;
        } else {
            var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!re.test(email)) {
                swal("Opsss...", "Tem certeza que você digitou um e-mail válido?!", "error");
                return false;
            }
        }

        if (!mensagem || mensagem == '') {
            swal("Opsss...", "Qual é a sua mensagem? o campo ficou vazio!", "error");
            return false;
        }

        enviarEmail(nome, email, mensagem)
            .then(function (retorno) {
                debugger;
                if (retorno.sucesso) {
                    swal("Tudo certo!", "Sua mensagem foi enviada com sucesso! :)", "success");
                    $('#contato_nome').val('');
                    $('#contato_email').val('');
                    $('#contato_mensagem').val('');
                } else {
                    swal("Opsss...", retorno.mensagem, "error");
                }
            });
    });

    function cadastrarEmail(_email) {
        return http()
            .post('/cadastrarEmail', { email: _email })
            .then(function(retorno) {
                return retorno;
            });
    }

    $('button.cadastrarEmail').click(function() {
        var email = $('#newsletter_email').val();

        if (!email || email == '') {
            swal("Opsss...", "Precisamos de um e-mail seu para te enviar as novidades!", "error");
            return false;
        } else {
            var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!re.test(email)) {
                swal("Opsss...", "Tem certeza que você digitou um e-mail válido?!", "error");
                return false;
            }
        }

        cadastrarEmail(email)
            .then(function (retorno) {
                if(retorno.sucesso) {
                    swal("Tudo certo!", "Em breve lhe mandaremos notícias! :)", "success");
                    $('#newsletter_email').val('');
                } else {
                    swal("Opsss...", retorno.mensagem, "error");
                }
            });
    });
});
