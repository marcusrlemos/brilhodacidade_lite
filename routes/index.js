function _index(req, res) {
    res.render('index', {})
}

var _emailService = require('../services/envioEmailService');

function _enviarEmail(req, res) {
    var nome = req.body.nome;
    var email = req.body.email;
    var mensagem = req.body.mensagem;

    try {
        _emailService.enviarEmailMensagem(nome, email, mensagem);
        return res.json({sucesso: true, mensagem: ''});
    } catch (e) {
        return res.json({
            sucesso: false,
            mensagem: 'Ocorreu um erro ao enviar sua mensagem, tente novamente ou envie diretamente para: contato@brilhodacidade.com.br.'
        });
    }
}

var _mysql = require('mysql');
var _db = require('../config/bancoDeDados');

function _cadastrarEmail(req, res) {
    var email = req.body.email;

    if (email && email.indexOf('@') >= 0) {
        try {
            var dataatual = new Date();
            dataatual = dataatual.getFullYear() + '-' + (dataatual.getMonth() + 1) + '-'
                      + (dataatual.getDate() < 10 ? '0' + dataatual.getDate() : dataatual.getDate() )+ ' '
                      + dataatual.getHours() + ':' + dataatual.getMinutes() + ':' + dataatual.getSeconds();

            _db
                .executar('insert into newsletterUsuario (email, dataCadastro) values (' + _mysql.escape(email) + ', ' + _mysql.escape(dataatual) + ')')
                .then(function () {
                    return res.json({sucesso: true, mensagem: ''});
                });
        } catch (e){
            return res.json({sucesso: false, mensagem: 'Ocorreu um erro ao cadastrar seu e-mail, tente novamente ou mais tarde, se preferir.'});
        }
    } else
        return res.json({sucesso: false, mensagem: 'O e-mail informado não era válido!'});
}

module.exports = function(app){
    app.get('/', _index);
    app.post('/cadastrarEmail', _cadastrarEmail);
    app.post('/enviarEmail', _enviarEmail);
};
