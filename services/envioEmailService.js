var _envioDeEmail = require('../config/envioDeEmail');

function _enviarEmailMensagem(nome, email, mensagem) {
    var dados = {
        nome: nome,
        email: email,
        mensagem: mensagem
    };

    var assunto = '[Brilho da Cidade] Contato pelo site';

    _envioDeEmail.enviarEmail("contato@brilhodacidade.com.br", assunto, 'contato', dados);
}

var envioEmailService = {
    enviarEmailMensagem: _enviarEmailMensagem
};

module.exports = envioEmailService;