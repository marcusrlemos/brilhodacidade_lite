DROP DATABASE IF EXISTS brilhodacidade_lite;

create database brilhodacidade_lite;

use brilhodacidade_lite;

CREATE TABLE newsletterUsuario (
	id int NOT NULL auto_increment,
	email varchar(50) NOT NULL,
	dataCadastro datetime NULL,
    PRIMARY KEY (id)
);